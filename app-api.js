const express = require("express");
const mysql = require("mysql2");
const bodyParser = require("body-parser");
const jsonParser = express.json();

const PORT = 5000;
const app = express()

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    database: "app-api-db",
    password: "mabagas11QBERT_verenei336_"
});

app.post("/posts/", jsonParser, (req, res) => {
    var obj = req.body;

    var arr = [obj.title, obj.content];

    const sqlQuery = `INSERT INTO infoapi (title,content) VALUES (?,?);`;

    connection.query(sqlQuery, arr, function(err, results) {
        if (err) {
            console.log(err);
            res.json(err);
        } else {
            console.log("Данные добавлены");
            res.status(200).json(arr);
        }
    })
});

app.get("/posts/all", jsonParser, (req, res) => {
    const sqlQuery = "SELECT * FROM infoapi";

    connection.query(sqlQuery, function(err, results) {
        if (err) {
            console.log(err);
            res.json(err);
        } else {
            console.log("Данные получены");
            res.status(200).json(results);
        }
    })
});

app.get('/posts/:ID', jsonParser, (req, res) => {
    const id = req.params['ID'];
    connection.query(`SELECT * FROM infoapi WHERE id = ${id}`, function(err, results) {
        if (err) {
            console.log(err);
            res.json(err);
        } else {
            console.log("Данные отправлены");
            res.status(200).json(results);
        }
    })
});

app.put('/posts/:ID', jsonParser, (req, res) => {
    const id = req.params['ID'];

    var obj = req.body;

    var arr = [obj.title, obj.content];

    connection.query(`UPDATE infoapi SET  title = ? ,content = ? WHERE id = ${id}`, arr, function(err, results) {
        if (err) {
            console.log(err);
            res.json(err);
        } else {
            console.log("Данные обновлены");
            res.status(200).json(results);
        }
    })
});

app.delete('/posts/:ID', jsonParser, (req, res) => {
    const id = req.params['ID'];
    connection.query(`DELETE FROM infoapi WHERE id = ${id}`, function(err, results) {
        if (err) {
            console.log(err);
            res.json(err);
        } else {
            console.log("Данные удалены");
            res.status(200).json(results);
        }
    })
});

async function startAPP() {
    try {
        await connection.connect()

        app.listen(PORT, () => console.log("server runned on 5000 port"))
    } catch (e) {
        console.log(e)
    }
}

startAPP()